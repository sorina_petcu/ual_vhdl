----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2021 10:42:23
-- Design Name: 
-- Module Name: Alu_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Alu_tb is
--  Port ( );
end Alu_tb;

architecture Behavioral of Alu_tb is
signal Clk, Rst, Cout: std_logic;
signal A, B: std_logic_vector(31 downto 0);
signal Y: std_logic_vector(63 downto 0);
signal OP: std_logic_vector(2 downto 0);
constant CLK_PERIOD : TIME := 10 ns;
begin
clk_gen: process
begin
    Clk <= '0';
    wait for (clk_period/2);
    Clk <= '1';
    wait for (clk_period/2);
end process;

sim: process
begin
    wait for (clk_period);
    Rst <= '1';
    wait for (clk_period);
    Rst <= '0';
    wait for (clk_period);
    
    A <= x"0000000A";
    B <= x"00000002";
    OP <= "110";
    wait for (25*clk_period);
    
    A <= x"0000000A";
    B <= x"00000002";
    OP <= "010";
    wait for (25*clk_period);
    
    A <= x"0000000A";
    B <= x"00000002";
    OP <= "001";
    wait for (25*clk_period);
    
    A <= x"0000000A";
    B <= x"00000002";
    OP <= "000";
    wait for (25*clk_period);
    
    A <= x"0000000A";
    B <= x"00000002";
    OP <= "100";
    wait for (25*clk_period);
    
    A <= x"0000000A";
    B <= x"00000002";
    OP <= "101";
    wait for (25*clk_period);
    wait;
end process;

alu: entity WORK.Alu32 port map (Clk, Rst, A, B, Y, OP, Cout);

end Behavioral;
