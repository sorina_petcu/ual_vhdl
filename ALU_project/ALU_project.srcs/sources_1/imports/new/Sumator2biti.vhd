----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.10.2020 12:17:50
-- Design Name: 
-- Module Name: Sumator2biti - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Sumator2biti is
 Port (X: in std_logic_vector(1 downto 0);
       Y: in std_logic_vector(1 downto 0);
       Tin: in std_logic;
       S: out std_logic_vector(1 downto 0);
       P: out std_logic;
       G: out std_logic );
end Sumator2biti;

architecture Behavioral of Sumator2biti is

signal g0,p0,g1,p1, T0 : std_logic;

begin

g0 <= X(0) and Y(0);
p0 <= X(0) or Y(0);
g1 <= X(1) and Y(1);
p1 <= X(1) or Y(1);

S(0) <= X(0) xor Y(0) xor Tin;
T0 <= g0 or (p0 and Tin);

S(1) <= X(1) xor Y(1) xor T0;
G <= g1 or (p0 and g1);
P <= p1 and p0;


end Behavioral;
