----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.11.2020 09:56:12
-- Design Name: 
-- Module Name: ADDN - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ADDN is
  generic(n: natural);
  Port (X   : in STD_LOGIC_VECTOR (n-1 downto 0);
        Y   : in STD_LOGIC_VECTOR (n-1 downto 0);
        Tin : in STD_LOGIC;
        S   : out STD_LOGIC_VECTOR (n-1 downto 0);
        Tout: out STD_LOGIC;
        OVF : out STD_LOGIC);
end ADDN;

architecture Behavioral of ADDN is
signal Z : STD_LOGIC_VECTOR (n downto 0);
begin
Z(0) <= Tin;
Sumn:    for i in 0 to n-1 generate
            S(i) <= X(i) xor Y(i) xor Z(i);
            Z(i+1) <= (X(i) and Y(i)) or ((X(i) or Y(i)) and Z(i));
        end generate;
 
 Tout <= Z(n);
 OVF <= Z(n) xor Z(n-1);
 
end Behavioral;

