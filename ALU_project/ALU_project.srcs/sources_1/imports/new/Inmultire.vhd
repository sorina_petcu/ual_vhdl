----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.11.2020 09:56:12
-- Design Name: 
-- Module Name: Inmultire - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Inmultire is
  generic(n : natural );
  Port (Clk   : in STD_LOGIC;
        Rst   : in STD_LOGIC;
        Start : in STD_LOGIC;
        X     : in STD_LOGIC_VECTOR (n-1 downto 0);
        Y     : in STD_LOGIC_VECTOR (n-1 downto 0);
        A     : out STD_LOGIC_VECTOR (n-1 downto 0);
        Q     : out STD_LOGIC_VECTOR (n-1 downto 0);
        Term  : out STD_LOGIC );
end Inmultire;

architecture Behavioral of Inmultire is

signal Q0Q_1 : STD_LOGIC_VECTOR(1 downto 0); --intrari UC
signal LoadQ, LoadB, LoadA , RstQ_1, RstA, ShrAQ, AddSub : STD_LOGIC; --iesiri UC
signal B_XOR : STD_LOGIC_VECTOR (n-1 downto 0); --intrare sumator-poarta XOR
--signal ADD_IN : STD_LOGIC_VECTOR (n-1 downto 0); --intrare ADDN
signal ADD_OUT : STD_LOGIC_VECTOR (n-1 downto 0); --iesire ADDN
signal Tout, OVF : STD_LOGIC; --iesire ADDN
signal B : STD_LOGIC_VECTOR (n-1 downto 0); --iesire FDN
signal Q_delay : STD_LOGIC; --iesire FD
signal QQ, Q_final : STD_LOGIC_VECTOR (n-1 downto 0); --iesiri SSRN

begin

U_CTRL: entity WORK.UC generic map (n => n) port map(
                Clk => Clk,
                Rst => Rst, 
                Start => Start,
                Q0Q_1 => Q0Q_1,
                LoadQ => LoadQ,
                LoadB => LoadB,
                LoadA => LoadA,
                RstQ_1 => RstQ_1,
                RstA  => RstA,
                ShrAQ => ShrAQ,
                AddSub => AddSub,
                Term  => Term);           
                  
FDN: entity WORK.FDN generic map (n => n) port map(
            Clk => Clk,
            D => X,
            Rst => Rst,
            CE => LoadB,
            Q => B);

XOR_GATE:   for i in 0 to n-1 generate
                B_XOR(i) <= AddSub xor B(i);
            end generate;
            
SUMATOR_n_1Biti: entity WORK.ADDN generic map (n => n) port map(
                  X => QQ,
                  Y => B_XOR,
                  Tin => AddSub,
                  S => ADD_OUT,
                  Tout => Tout,
                  OVF => OVF);
                  
SHIFT1: entity WORK.SRRN generic map (n=>n) port map(
                Clk => Clk,
                D => ADD_OUT,
                SRI => QQ(n-1),
                Rst => RstA,
                Load => LoadA,
                CE => ShrAQ,
                Q => QQ);
                
SHIFT2: entity WORK.SRRN generic map (n=>n) port map(
                Clk => Clk,
                D => Y,
                SRI => QQ(0),
                Rst => Rst,
                Load => LoadQ,
                CE => ShrAQ,
                Q => Q_final);
                
DELAY: entity WORK.FD  port map(
                Clk => Clk,
                Rst => RstQ_1,
                CE => ShrAQ,
                D => Q_final(0),
                Q => Q_delay);
                
Q0Q_1(1) <= Q_final(0);
Q0Q_1(0) <= Q_delay;
A <= QQ;
Q <= Q_final;          

end Behavioral;
