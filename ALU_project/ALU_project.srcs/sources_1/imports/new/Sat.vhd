----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.10.2020 12:24:43
-- Design Name: 
-- Module Name: Sat - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Sat is
    Port (X: in std_logic_vector(31 downto 0);
          Y: in std_logic_vector(31 downto 0);
          Tin: in std_logic;
          Suma: out std_logic_vector(31 downto 0);
          Tout: out std_logic);
end Sat;

architecture Behavioral of Sat is

signal t2,t4,t6,t8, t10, t12, t14, t16, t18, t20, t22, t24, t26, t28, t30, P01,G01,P23,G23,P45,G45,P67,G67, P89, G89, P1011, G1011, P1213, G1213, P1415, G1415, P1617, G1617, P1819, G1819, P2021, G2021, P2223, G2223, P2425, G2425, P2627, G2627, P2829, G2829, P3031, G3031, SumAux: std_logic;

begin

S0: entity work.Sumator2biti port map(X=>X(1 downto 0), Y=>Y(1 downto 0),Tin=>Tin,S=>Suma(1 downto 0), P=>P01,G=>G01);
S1: entity work.Sumator2biti port map(X=>X(3 downto 2), Y=>Y(3 downto 2),Tin=>t2,S=>Suma(3 downto 2), P=>P23,G=>G23);
S2: entity work.Sumator2biti port map(X=>X(5 downto 4), Y=>Y(5 downto 4),Tin=>t4,S=>Suma(5 downto 4), P=>P45,G=>G45);
S3: entity work.Sumator2biti port map(X=>X(7 downto 6), Y=>Y(7 downto 6),Tin=>t6,S=>Suma(7 downto 6), P=>P67,G=>G67);
S4: entity work.Sumator2biti port map(X=>X(9 downto 8), Y=>Y(9 downto 8),Tin=>t8,S=>Suma(9 downto 8), P=>P89,G=>G89);
S5: entity work.Sumator2biti port map(X=>X(11 downto 10), Y=>Y(11 downto 10),Tin=>t10,S=>Suma(11 downto 10), P=>P1011,G=>G1011);
S6: entity work.Sumator2biti port map(X=>X(13 downto 12), Y=>Y(13 downto 12),Tin=>t12,S=>Suma(13 downto 12), P=>P1213,G=>G1213);
S7: entity work.Sumator2biti port map(X=>X(15 downto 14), Y=>Y(15 downto 14),Tin=>t14,S=>Suma(15 downto 14), P=>P1415,G=>G1415);
S8: entity work.Sumator2biti port map(X=>X(17 downto 16), Y=>Y(17 downto 16),Tin=>t16,S=>Suma(17 downto 16), P=>P1617,G=>G1617);
S9: entity work.Sumator2biti port map(X=>X(19 downto 18), Y=>Y(19 downto 18),Tin=>t18,S=>Suma(19 downto 18), P=>P1819,G=>G1819);
S10: entity work.Sumator2biti port map(X=>X(21 downto 20), Y=>Y(21 downto 20),Tin=>t20,S=>Suma(21 downto 20), P=>P2021,G=>G2021);
S11: entity work.Sumator2biti port map(X=>X(23 downto 22), Y=>Y(23 downto 22),Tin=>t22,S=>Suma(23 downto 22), P=>P2223,G=>G2223);
S12: entity work.Sumator2biti port map(X=>X(25 downto 24), Y=>Y(25 downto 24),Tin=>t24,S=>Suma(25 downto 24), P=>P2425,G=>G2425);
S13: entity work.Sumator2biti port map(X=>X(27 downto 26), Y=>Y(27 downto 26),Tin=>t26,S=>Suma(27 downto 26), P=>P2627,G=>G2627);
S14: entity work.Sumator2biti port map(X=>X(29 downto 28), Y=>Y(29 downto 28),Tin=>t28,S=>Suma(29 downto 28), P=>P2829,G=>G2829);
S15: entity work.Sumator2biti port map(X=>X(31 downto 30), Y=>Y(31 downto 30),Tin=>t30,S=>Suma(31 downto 30), P=>P3031,G=>G3031);

t2<=G01 or (P01 and Tin);
t4<= G23 or (P23 and t2);
t6<= G45 or (P45 and t4);
t8<= G67 or (P67 and t6);
t10<= G89 or (P89 and t8);
t12<=G1011 or (P1011 and t10);
t14<=G1213 or (P1213 and t12);
t16<=G1415 or (P1415 and t14);
t18<=G1617 or (P1617 and t16);
t20<=G1819 or (P1819 and t18);
t22<=G2021 or (P2021 and t20);
t24<=G2223 or (P2223 and t22);
t26<=G2425 or (P2425 and t24);
t28<=G2627 or (P2627 and t26);
t30<=G2829 or (P2829 and t28);
Tout<=G3031 or (P3031 and t30);

end Behavioral;
