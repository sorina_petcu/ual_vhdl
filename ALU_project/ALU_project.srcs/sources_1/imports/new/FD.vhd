----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.11.2020 09:56:12
-- Design Name: 
-- Module Name: FD - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FD is
  Port (Clk : in STD_LOGIC;
        Rst : in STD_LOGIC;
        CE  : in STD_LOGIC;
        D   : in STD_LOGIC;
        Q   : out STD_LOGIC);
end FD;

architecture Behavioral of FD is

signal Q_Aux : STD_LOGIC := '0';

begin

process(Clk)
begin

    if RISING_EDGE(Clk) then
        if (Rst = '1') then
            Q_Aux <= '0';
        elsif (CE = '1') then
            Q_Aux <= D;
         else
            Q_Aux <= Q_Aux;
        end if;
    end if;
end process;
Q <= Q_Aux;
end Behavioral;
