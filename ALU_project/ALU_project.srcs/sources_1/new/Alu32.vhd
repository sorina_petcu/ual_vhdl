----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.12.2020 13:45:28
-- Design Name: 
-- Module Name: Alu32 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Alu32 is
Port ( Clk : in STD_LOGIC;
       Rst: in std_logic;
       A : in  std_logic_vector (31 downto 0);
       B : in  std_logic_vector(31 downto 0);
       Y : out  std_logic_vector(63 downto 0);
       OP : in  std_logic_vector(2 downto 0);
       --zero   : out std_logic; --zero flag
       Cout : out  STD_LOGIC); --carry out
end Alu32;

architecture Behavioral of Alu32 is
signal addres, Amul, Qmul, andres, orres, rolres, rorres: std_logic_vector(31 downto 0);
signal mulres: std_logic_vector(63 downto 0);
signal Start, Term: std_logic;
begin
   Start <= '1' when OP = "110" else '0';
   mulres <= Amul & Qmul when Term = '1';
   process(A, B, OP, andres, orres, addres, rorres, rolres, mulres)
   begin
      case OP is
        when "000" => Y <= x"00000000" & andres;
        when "001" => Y <= x"00000000" & orres;
        when "010" => Y <= x"00000000" & addres;
        --when "011" => Y <= A - B;
        when "100" => Y <= x"00000000" & rorres; -- ROR
        when "101" => Y <= x"00000000" & rolres;--ROL
        when "110" => Y <= mulres;--Booth
        when others => NULL;
     
   end case;
   end process;
   
 SAT : entity WORK.SAT port map(A, B, '0', addres, Cout);
 mul: entity WORK.Inmultire generic map (n => 32) port map(Clk, Rst, Start, A, B, Amul, Qmul, Term);
 aand: entity WORK.And32bit port map (A, B, andres);
 oor: entity WORK.Or32bit port map (A, B, orres);
 rrol: entity WORK.LeftShift32bit port map(A, rolres);
 rror: entity WORK.RightShift32bit port map(A, rorres);

end Behavioral;
