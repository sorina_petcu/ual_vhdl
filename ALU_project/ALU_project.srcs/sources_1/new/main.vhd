----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2021 15:07:45
-- Design Name: 
-- Module Name: main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
  Port ( Clk: in std_logic;
         Rst: in std_logic;
         OP: in std_logic_vector(2 downto 0);
         Cout: out std_logic;
         An: out std_logic_vector(7 downto 0);
         Seg: out std_logic_vector(7 downto 0));
end main;

architecture Behavioral of main is
signal A: std_logic_vector(31 downto 0) := x"0000000A";
signal B: std_logic_vector(31 downto 0) := x"00000002";
signal Data: std_logic_vector(31 downto 0);
signal res: std_logic_vector(63 downto 0);
signal Rst1: std_logic;
begin

Data <= res(31 downto 0);

--process
--begin
--    if OP = "110" then
--        Data <= res(63 downto 32);
--        wait for 1000000000ns;
--        Data <= res(31 downto 0);
--    end if;
--end process;

alu: entity WORK.Alu32 port map(Clk, Rst1, A, B, res, OP, Cout);

display: entity WORK.displ7seg port map (Clk, Rst1, Data, An, Seg);

buton: entity WORK.filtru_buton port map (Clk, Rst, Rst, Rst1);

end Behavioral;
