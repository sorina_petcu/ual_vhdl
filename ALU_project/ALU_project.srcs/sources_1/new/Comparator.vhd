----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.12.2020 08:33:15
-- Design Name: 
-- Module Name: Comparator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity COMPARATOR_SOURCE is
    Port ( FN : in  STD_LOGIC_VECTOR (1 downto 0);
           G,L,E : out  STD_LOGIC);
end COMPARATOR_SOURCE;

architecture Behavioral of COMPARATOR_SOURCE is

begin
process (FN)
begin

G <= '0';
L <= '0';
E <= '0';
if (FN(0) < FN(1)) then
	L <= '1';

elsif (FN(0) > FN(1)) then
	G <= '1';

else
	E <= '1';
end if;
end process;
end Behavioral;
