----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.12.2020 08:12:01
-- Design Name: 
-- Module Name: Mux4_to_1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux4_to_1 is
port(
 
     Shift, Bool, Arith, Cmp : in STD_LOGIC_VECTOR(31 downto 0);
     FN0,FN1: in STD_LOGIC;
     Y: out STD_LOGIC_VECTOR(31 downto 0)
  );

end Mux4_to_1;

architecture Behavioral of Mux4_to_1 is
begin
process (Shift, Bool, Arith, Cmp, FN0,FN1) is
begin
  if (FN0 ='0' and FN1 = '0') then
      Y <= Shift;
  elsif (FN0 ='1' and FN1 = '0') then
      Y <= Bool;
  elsif (FN0 ='0' and FN1 = '1') then
      Y <= Arith;
  else
      Y <= Cmp;
  end if;
  
 end process;
end Behavioral;
