----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.01.2021 09:19:46
-- Design Name: 
-- Module Name: Or32bit - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Or32bit is
 Port ( A_in : in std_logic_vector (31 downto 0);
        B_in : in std_logic_vector (31 downto 0);
        res : out std_logic_vector (31 downto 0)
  );
end Or32bit;

architecture Behavioral of Or32bit is

begin

res <= A_in or B_in;

end Behavioral;
