----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.12.2020 15:39:14
-- Design Name: 
-- Module Name: RightShift32Bit - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RightShift32Bit is
 Port ( A_in : in std_logic_vector (31 downto 0);
        res : out std_logic_vector (31 downto 0)
  );
end RightShift32Bit;

architecture Behavioral of RightShift32Bit is

begin

res(31 downto 0) <= '0' & A_in(30 downto 0); --ROR
--res(31) <= '0';

end Behavioral;
